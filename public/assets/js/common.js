//slick slider header
$(document).ready(function(){
  $('.slider-slick').slick({
  	infinite: true,
  	slidesToShow: 1,
  	slidesToScroll: 1,
  	dots: true,
  	autoplay: false,
  	autoplaySpeed: 5000,
  	speed: 500,
  	fade: true,
  	cssEase: 'linear',
    swipe: true,
  	appendDots: $('.dots'),
  	prevArrow: '<button type="button" class="slick-prev">Previous</button>',
		nextArrow: '<button type="button" class="slick-next">Next</button>'
  });
});

// add to favorites
$('.add-to-bookmarks').click(function(e) {
    var bookmarkURL = window.location.href;
    var bookmarkTitle = document.title;

    if ('addToHomescreen' in window && addToHomescreen.isCompatible) {
      // Mobile browsers
      addToHomescreen({ autostart: false, startDelay: 0 }).show(true);
    } else if (window.sidebar && window.sidebar.addPanel) {
      // Firefox <=22
      window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
    } else if ((window.sidebar && /Firefox/i.test(navigator.userAgent)) || (window.opera && window.print)) {
      // Firefox 23+ and Opera <=14
      $(this).attr({
        href: bookmarkURL,
        title: bookmarkTitle,
        rel: 'sidebar'
      }).off(e);
      return true;
    } else if (window.external && ('AddFavorite' in window.external)) {
      // IE Favorites
      window.external.AddFavorite(bookmarkURL, bookmarkTitle);
    } else {
      // Other browsers (mainly WebKit & Blink - Safari, Chrome, Opera 15+)
      alert('Press ' + (/Mac/i.test(navigator.platform) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
    }

    return false;
  });

var currentSlick = 0,
  notStart = false;
//slick slider tabs
var destroySlick = function(i) {
  var slider = $(".tabs").find(".tabs-content").find(".tabs-content-item").eq(i).find(".tabs-slider");
  if (slider) $(slider).slick('unslick');
}
var triggerSlick = function(container) {
  $(container).slick({
  	infinite: true,
  	slidesToShow: 1,
  	slidesToScroll: 1,
  	dots: false,
  	autoplay: false,
  	autoplaySpeed: 3000,
  	speed: 500,
    adaptiveHeight: true,
    mobileFirst: true,
    swipe: true
  });
};

//tabs
(function($){				
  jQuery.fn.lightTabs = function(options){
  	var createTabs = function() {
      tabs = this;
      i = 0;
      
      showPage = function(i) {
        console.log(tabs)
        $(tabs).find(".tabs-content").find(".tabs-content-item").hide();
        $(tabs).find(".tabs-content").find(".tabs-content-item").eq(i).show();
        $(tabs).find(".menu").children("li").removeClass("active");
        $(tabs).find(".menu").children("li").eq(i).addClass("active");
        var slider = $(tabs).find(".tabs-content").find(".tabs-content-item").eq(i).find(".tabs-slider");
        if (notStart) destroySlick(currentSlick);
        triggerSlick(slider);
        currentSlick = i;
      }
          
      showPage(0);				
      
      $(tabs).find(".menu").children("li").each(function(index, element){
        $(element).attr("data-page", i);
        i++;                        
      });
      
      $(tabs).find(".menu").children("li").click(function(){
        showPage(parseInt($(this).attr("data-page")));       
      });				
    };		
  return this.each(createTabs);
  };	
})(jQuery);


$(".popup-closebutton").click(function() {
  $(".popup-container").removeClass("active");
  $("#popup").find(".item").removeClass("active");
});

$(".action-open").click(function() {
  popupOpen("action");
})
$(".widget-open").click(function() {
  popupOpen("widget");
})
$(".share-open").click(function() {
  popupOpen("share");
})
$(".oferta-open").click(function() {
  popupOpen("oferta");
})

var popupOpen = function(type) {  
  var p = $("#popup");
  $(p).removeClass().addClass("popup");
  $(p).find("item").removeClass("active");
  if (type == "action") {
    $(p).addClass("small").find(".item1").addClass("active");
  }
  if (type == "widget") {
    $(p).addClass("widget").find(".item2").addClass("active");    
  }
  if (type == "share") {
    $(p).addClass("small").find(".item3").addClass("active");
  }
  if (type == "oferta") {
    $(p).find(".item4").addClass("active");
  }
  $(".popup-container").addClass("active");
}

// фиксированная плашка в верхней части на десктопах


$(".share").click(function() {
  var href = window.location.href,
    title = document.title;
  if ($(this).attr("data") == "fb") {
    myWin = open("http://www.facebook.com/sharer.php?u=" + href, "displayWindow","width=520,height=300,left=400,top=300,status=no,toolbar=no,menubar=no");
  } else if ($(this).attr("data") == "vk") {
    myWin = open("http://vkontakte.ru/share.php?url=" + href, "displayWindow","width=520,height=300,left=400,top=300,status=no,toolbar=no,menubar=no");
  } else if ($(this).attr("data") == "tw") { 
    myWin = open("http://twitter.com/share?text=@webdesign&amp;url=" + href, "displayWindow","width=520,height=300,left=400,top=300,status=no,toolbar=no,menubar=no");
  } else if ($(this).attr("data") == "in") { 
    myWin = open("https://www.instagram.com/lastick.ru/");
  } else if ($(this).attr("data") == "ok") {
    myWin = open("http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=" + href, "&st.comments=" + title);
  } else if ($(this).attr("data") == "tg") {
    myWin = open("https://telega.cc/share/url?url=" + href, "&text=" + title);
  } else if ($(this).attr("data") == "wa") {
    myWin = open("whatsapp://send?text=" + href);   
  } 
  return false;
})



$(document).ready(function(){
  $(".tabs--two").lightTabs();
  $(".tabs--one").lightTabs();
 
  triggerSlick();

  notStart = true;


});
