export interface ICertRequest {
  type: string;
  name: string;
  email: string;
  message?: string;
  phone: string;
}

export interface ISocialItem {
  name: string;
  short_name: string;
  formatted_name?: string;
  url?: string;
  share_url?: string;
  mobileOnly?: boolean;
  icon: string;
}