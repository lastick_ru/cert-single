import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexPageComponent } from './components/pages/index-page/index-page.component';

const routes: Routes = [
  {
    path: '', component: IndexPageComponent
  },
  {
    path: '**',
    resolve: {
      path: IndexPageComponent
    },
    component: IndexPageComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRouterModule { }
