import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CertificateService } from 'src/app/services/certificate.service';

@Component({
  selector: 'app-doc',
  templateUrl: './doc.component.html',
  styleUrls: ['./doc.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class DocComponent implements OnInit {

  constructor(
    public service: CertificateService
  ) { }

  ngOnInit() {
  }

  close(){
    this.service.docIsOpen = false;
  }
}
