import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/views/header/header.component';
import { FooterComponent } from './components/views/footer/footer.component';
import { TabsComponent } from './components/views/tabs/tabs.component';

import { AppRouterModule } from './router';
import { FormsModule } from '@angular/forms';
import { CertificateService } from '../app/services/certificate.service';
import { HttpClientModule } from '@angular/common/http';
import { SocialService } from './services/social.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { DocComponent } from './components/views/doc/doc.component';
import { IndexPageComponent } from './components/pages/index-page/index-page.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SectionItemComponent } from './components/views/section-item/section-item.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    TabsComponent,
    ClickOutsideDirective,
    DocComponent,
    IndexPageComponent,
    SectionItemComponent
  ],
  imports: [
    BrowserModule,
    AppRouterModule,
    FormsModule,
    HttpClientModule,
    SlickCarouselModule,
    NgbModule

  ],
  providers: [CertificateService, SocialService, DeviceDetectorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
