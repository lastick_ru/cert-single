import { Injectable } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ISocialItem } from '../interfaces';


@Injectable()
export class SocialService {
  public data: ISocialItem[] = [
    {
      name: 'facebook',
      short_name: 'fb',
      formatted_name: 'Facebook',
      url: '#',
      share_url: 'http://www.facebook.com/sharer.php?u=',
      icon: '/assets/svg/facebook.svg',
    },
    {
      name: 'vkontakte',
      short_name: 'vk',
      formatted_name: 'Вконтакте',
      url: '#',
      share_url: 'http://vkontakte.ru/share.php?url=',
      icon: '/assets/svg/vk.svg',
    },
    {
      name: 'instagram',
      short_name: 'insta',
      formatted_name: 'Instagram',
      url: '#',
      //share_url: 'https://www.instagram.com/share/url?url=',
      icon: '/assets/svg/insta.svg',
    },
    {
      name: 'telegram',
      short_name: 'tg',
      formatted_name: 'Telegram',
      url: '#',
      share_url: 'https://telega.cc/share/url?url=',
      icon: '/assets/svg/telegram.svg',
    },
    {
      name: 'whatsup',
      short_name: 'ws',
      formatted_name: 'WhatsApp',
      url: '#',
      share_url: 'whatsapp://send?text=',
      icon: '/assets/svg/whatsup.svg',
      mobileOnly: false,
    }
  ];

  constructor(private device: DeviceDetectorService) {}

  share(item: ISocialItem, href = true) {
    window.open(
      `${item.share_url + (href ? location.href : location.origin)}`,
      'displayWindow',
      this.device.isDesktop()
        ? `width=520,height=300,left=400,top=300,status=no,toolbar=no,menubar=no`
        : null
    );
  }

}



